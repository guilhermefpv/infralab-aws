terraform {
  #required_version = ">= 1.0.0"
  
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
 #backend.tf
}


# Configure the AWS Provider
provider "aws" {
  region = var.region
}