terraform {
  # Configure a Remote Backend with AWS S3 and DynamoDB state locking in Terraform
  backend "s3" {
    bucket = "tfbackend-state"
    key            = "global/s3/terraform.tfstate"
    region= "us-east-1"
    dynamodb_table = "tfstate-lock"
    encrypt        = true
  }
}
