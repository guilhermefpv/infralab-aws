variable "region" {
  description = "region"
  type        = string
  default     = "us-east-1"
}

# https://cloud-images.ubuntu.com/locator/ec2/
variable "instance_type_bastion" {
  description = "Value of the Name tag for the EC2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "bastion" {
  description = "Value of the Name tag for the EC2 instance type"
  type        = string
  default     = "t2.micro"
}

variable "s3_bucket_name" {
  description = "The name of the S3 bucket. Must be globally unique."
  type        = string
  default = "tfbackend-state"
}

variable "table_name" {
  description = "The name of the DynamoDB table. Must be unique in this AWS account."
  type        = string
  default = "tfstate-lock" 
}

variable "ami_id" {
    type = string
    default = "ami-0c7217cdde317cfec"
}

variable "availability_zones" {
  type    = list(string)
  #default = ["us-east-1a", "us-east-1b", "us-east-1c"]
  default = ["us-east-1a"]
}

variable "vpc_cidr" {
    type = string
    default = "10.0.0.0/16"
}

variable "private_subnets" {
    type = list(string)
    #default = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
    default = ["10.0.1.0/24"]
}

variable "public_subnets" {
    type = list(string)
    #default = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]
    default = ["10.0.101.0/24"]
}

variable "master_node_count" {
    type = number
    default = 1
}

variable "worker_node_count" {
    type = number
    default = 2
}

variable "ssh_user" {
    type = string
    default = "ubuntu"
}

variable "master_instance_type" {
    type = string
    default = "t3.small"
}

variable "worker_instance_type" {
    type = string
    default = "t3.micro"
}
